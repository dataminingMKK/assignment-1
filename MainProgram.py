from DataReader import readData
from DataReader import personDict2
import pickle
from DataTransformation import transformData
from CombineXDays import checkDaysInRow
from CombineXDays import get_benchmark_score





personDict = {}

def main():

    # #maakt éénmalig een pickle en slaat deze op onder de naam personPick.pickle.
    # #Na het aanmaken van de pickle heb je dit niet meer nodig, tenzij de input data veranderd
    # readData()
    # with open('personPick.pickle', 'wb') as p:
    #     pickle.dump(personDict2, p)


    # Opent de pickle en stopt alle gegevens in onze dictionary: personDict
    #dit moet in comments als je een pickle opnieuw aanmaakt
    with open('personPick.pickle', "rb") as p:
        personDict = pickle.load(p)

    transformData(personDict)
    checkDaysInRow(personDict)
    benchmark_score = get_benchmark_score()
    print('benchmark', benchmark_score)


main()

