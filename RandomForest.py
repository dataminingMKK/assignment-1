import pandas as pd
from sklearn.ensemble import RandomForestRegressor, ExtraTreesRegressor
from sklearn.preprocessing import Imputer
from sklearn.grid_search import GridSearchCV
from sklearn.cross_validation import ShuffleSplit

from sklearn.learning_curve import validation_curve
import matplotlib.pyplot as plt
import numpy as np


if __name__ == '__main__':
    file = pd.read_csv("C:/Users/Kylie/Documents/school/Data Mining/output.csv")

    origin_data = file[file.moodDayX.notnull()]
    print(origin_data.shape)
    real_value = origin_data.ix[:,23]
    data = origin_data.ix[:,4:23]

    niks = data.ix[:,0:4]
    fill = data.ix[:,4:19]

    fill = fill.fillna(0)

    data = pd.concat([niks,fill], axis=1)

    imp = Imputer(missing_values='NaN', strategy='mean', axis=0)
    imp.fit(data)
    X = imp.transform(data)
    y = origin_data['moodDayX']

    param_name = "max_features"
    param_range = range(1, X.shape[1]+1)

    for Forest, color, label in [(RandomForestRegressor, "g", "RF"),
                                 (ExtraTreesRegressor,"r", "ET's" )]:
        print("test")
        _, test_scores = validation_curve(
            Forest(n_estimators = 100, n_jobs=-1), X, y,
            cv = ShuffleSplit(n=len(X), n_iter=10, test_size=0.2, random_state=0),
            param_name=param_name, param_range=param_range,
            scoring="mean_absolute_error")
        test_scores_mean = np.mean(-test_scores, axis=1)
        plt.plot(param_range,test_scores_mean, label=label, color=color)

    plt.xlabel(param_name)
    plt.xlim(1, max(param_range))
    plt.ylabel("MAE")
    plt.legend(loc="best")
    plt.show()


    cv = ShuffleSplit(n=len(X), n_iter=10, test_size=0.2, random_state=0)
    params = {'n_estimators': [75,100,125],
              'max_features': [4,5,6],
              'min_samples_split': [3,5,7],
              'min_samples_leaf': [1,2,3,4]}

    clf = GridSearchCV(RandomForestRegressor(), params, cv=cv, verbose =1 , scoring='mean_absolute_error', n_jobs = -1)
    grid = clf.fit(X,y)

    print(grid.best_params_)
    print(grid.best_score_)
    print(grid.best_estimator_)