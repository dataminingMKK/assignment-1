from statistics import mean



def transformData(personDict):
    for key, values in personDict.items():
        for k,v in values.days.items():
            averageVariables(v.mood)
            averageVariables(scaleData(v.arousal))
            averageVariables(scaleData(v.valence))
            averageVariables(v.activity)
            sumVariables(v.screen)
            sumVariables(v.call)
            sumVariables(v.sms)
            sumVariables(v.builtin)
            sumVariables(v.communication)
            sumVariables(v.entertainment)
            sumVariables(v.finance)
            sumVariables(v.game)
            sumVariables(v.office)
            sumVariables(v.other)
            sumVariables(v.social)
            sumVariables(v.travel)
            sumVariables(v.unknown)
            sumVariables(v.utilities)
            sumVariables(v.weather)



#schaalt de data naar 0-4
def scaleData(activity):
    if activity:
        for index, number in enumerate(activity):
            activity[index] += 2


def averageVariables(activity):
    if activity:
        means = mean(activity)
        del activity[:]
        activity.append(means)



def sumVariables(activity):
    if activity:
        sums = sum(activity)
        del activity[:]
        activity.append(sums)



