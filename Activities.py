class Activities():

    def __init__(self):
        self.mood = []
        self.arousal = []
        self.valence = []
        self.activity = []
        self.screen = []
        self.call = []
        self.sms = []
        self.builtin = []
        self.communication = []
        self.entertainment = []
        self.finance = []
        self.game = []
        self.office = []
        self.other = []
        self.social= []
        self.travel = []
        self.unknown = []
        self.utilities = []
        self.weather = []



    def addAttribute(self, var, value):
        if value != "NA":
            if var == "mood" and 1 <= value <= 10 :
                self.mood.append(float(value))
            elif var == "circumplex.arousal" and -2 <= value <= 2:
                self.arousal.append(float(value))
            elif var == "circumplex.valence" and -2 <= value <= 2:
                self.valence.append(float(value))
            elif var == "activity" and 0 <= value <= 1:
                self.activity.append(float(value))
            elif var == "screen" and value >= 0:
                self.screen.append(float(value))
            elif var == "call" and value == 0 or value == 1:
                self.call.append(float(value))
            elif var == "sms" and value == 0 or value == 1:
                self.sms.append(float(value))
            elif var == "appCat.builtin" and value >= 0:
                self.builtin.append(float(value))
            elif var == "appCat.communication" and value >= 0:
                self.communication.append(float(value))
            elif var == "appCat.entertainment" and value >= 0:
                self.entertainment.append(float(value))
            elif var == "appCat.finance" and value >= 0:
                self.finance.append(float(value))
            elif var == "appCat.game" and value >= 0:
                self.game.append(float(value))
            elif var == "appCat.office" and value >= 0:
                self.office.append(float(value))
            elif var == "appCat.other" and value >= 0:
                self.other.append(float(value))
            elif var == "appCat.social" and value >= 0:
                self.social.append(float(value))
            elif var == "appCat.travel" and value >= 0:
                self.travel.append(float(value))
            elif var == "appCat.unknown" and value >= 0:
                self.unknown.append(float(value))
            elif var == "appCat.utilities" and value >= 0:
                self.utilities.append(float(value))
            elif var == "appCat.weather" and value >= 0:
                self.weather.append(float(value))