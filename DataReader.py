from Headers import *
import csv
from local_settings import *
from Person import *
from Activities import *
from datetime import datetime


personDict2 = {}
headers = []


def readData():
    with open(input_file) as dataFile:
        reader = csv.reader(dataFile, delimiter = ",")
        headers = next(reader)
        if headers:
            readHeaders(headers)

        for row in reader:
            uniqueKey = row[headers.index(id)]
            if uniqueKey not in personDict2:
                personDict2[uniqueKey] = makePersonObject()
            addToPerson(row)





def getHeaders(name):
    headers.append(name)

def readHeaders(headerRow):
    for col in headerRow:
        getHeaders(col)


def makePersonObject():
    dayDict = {}
    return Person(dayDict)



def addToPerson(row):
    readDate = datetime.strptime(row[headers.index(timeHeader)], "%Y-%m-%d %H:%M:%S.%f").date()
    if readDate not in personDict2[row[headers.index(id)]].days:
        personDict2[row[headers.index(id)]].days[readDate] = makeActivityObject()
    addToActivity(row, readDate)

def makeActivityObject():
    return Activities()

def addToActivity(row, readDate):
    var = row[headers.index(variable)]
    val = row[headers.index(value)]
    personDict2[row[headers.index(id)]].days[readDate].addAttribute(var, val)

