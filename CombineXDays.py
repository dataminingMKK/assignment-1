from datetime import timedelta
from GroupedByDays import *
from datetime import datetime
from DataTransformation import averageVariables
from DataTransformation import sumVariables
import pandas as pd
import numpy as np
from statistics import mean
from sklearn import metrics

DAY_AMOUNT = 1
dailyGroupedData = {}


#loop in methode omdat de personDict hier anders is dan in het begin
#dayValues is de dict met datum:activitiesObject
#Deze methode kijkt of er wel x dagen volgen na x=0
#DAY_AMOUNT - 2 want de eerste dag is er sowieso al, en hij loopt vanaf 0
def checkDaysInRow(personDict):
    for idKey, dayValues in personDict.items():
        for dateKey, activityObject in sorted(dayValues.days.items()):
            uniqueKey = (idKey, dateKey)
            dailyGroupedData[uniqueKey] = makeGroupedByDays(activityObject, idKey, dateKey)
            for i in range(DAY_AMOUNT):
                if dateKey + timedelta(days=i) in dayValues.days:
                    updateGroupedByDate(dayValues.days[dateKey+timedelta(days=i)], uniqueKey)
            makeLine(dailyGroupedData[uniqueKey])
            if dateKey + timedelta(days=DAY_AMOUNT+1) in dayValues.days:
                a = dayValues.days[dateKey + timedelta(days=DAY_AMOUNT+1)].mood
                if a:
                    dailyGroupedData[uniqueKey].moodDayX.append(mean(a))
    a = 1

    makePanda(makeListDict())




#maakt een nieuw groupedByDays aan
def makeGroupedByDays(activityObject, idKey, dateKey):
    groupedByDays = GroupedByDays()
    groupedByDays.id = idKey
    groupedByDays.firstDay = dateKey
    return groupedByDays

#Sorry lelijk
def updateGroupedByDate(activityObject, key):
    dailyGroupedData[key].amountOfDays += 1
    if activityObject.mood:
        dailyGroupedData[key].mood.append(activityObject.mood[0])
    if activityObject.arousal:
        dailyGroupedData[key].arousal.append(activityObject.arousal[0])
    if activityObject.valence:
        dailyGroupedData[key].valence.append(activityObject.valence[0])
    if activityObject.activity:
        dailyGroupedData[key].activity.append(activityObject.activity[0])
    if activityObject.screen:
        dailyGroupedData[key].screen.append(activityObject.screen[0])
    if activityObject.call:
        dailyGroupedData[key].call.append(activityObject.call[0])
    if activityObject.sms:
        dailyGroupedData[key].sms.append(activityObject.sms[0])
    if activityObject.builtin:
        dailyGroupedData[key].builtin.append(activityObject.builtin[0])
    if activityObject.communication:
        dailyGroupedData[key].communication.append(activityObject.communication[0])
    if activityObject.entertainment:
        dailyGroupedData[key].entertainment.append(activityObject.entertainment[0])
    if activityObject.finance:
        dailyGroupedData[key].finance.append(activityObject.finance[0])
    if activityObject.game:
        dailyGroupedData[key].game.append(activityObject.game[0])
    if activityObject.office:
        dailyGroupedData[key].office.append(activityObject.office[0])
    if activityObject.other:
        dailyGroupedData[key].other.append(activityObject.other[0])
    if activityObject.social:
        dailyGroupedData[key].social.append(activityObject.social[0])
    if activityObject.travel:
        dailyGroupedData[key].travel.append(activityObject.travel[0])
    if activityObject.unknown:
        dailyGroupedData[key].unknown.append(activityObject.unknown[0])
    if activityObject.utilities:
        dailyGroupedData[key].utilities.append(activityObject.utilities[0])
    if activityObject.weather:
        dailyGroupedData[key].weather.append(activityObject.weather[0])


def makeLine(xDaysNumbers):
    averageVariables(xDaysNumbers.mood)
    averageVariables(xDaysNumbers.arousal)
    averageVariables(xDaysNumbers.valence)
    averageVariables(xDaysNumbers.activity)
    sumVariables(xDaysNumbers.screen)
    sumVariables(xDaysNumbers.call)
    sumVariables(xDaysNumbers.sms)
    sumVariables(xDaysNumbers.builtin)
    sumVariables(xDaysNumbers.communication)
    sumVariables(xDaysNumbers.entertainment)
    sumVariables(xDaysNumbers.finance)
    sumVariables(xDaysNumbers.game)
    sumVariables(xDaysNumbers.office)
    sumVariables(xDaysNumbers.other)
    sumVariables(xDaysNumbers.social)
    sumVariables(xDaysNumbers.travel)
    sumVariables(xDaysNumbers.unknown)
    sumVariables(xDaysNumbers.utilities)
    sumVariables(xDaysNumbers.weather)

# c_dict = {k: pd.DataFrame(v) for k, v in groups.groups.iteritems() }

def makeListDict():
    pandasDict = {}
    for k,v in dailyGroupedData.items():
        varList = []
        if v.id:
            varList.append(v.id)
        else: varList.append("")
        if v.firstDay:
            varList.append(v.firstDay)
        else: varList.append("")
        if v.amountOfDays:
            varList.append(v.amountOfDays)
        else: varList.append("")
        if v.mood:
            varList.append(v.mood[0])
        else: varList.append("")
        if v.arousal:
            varList.append(v.arousal[0])
        else: varList.append("")
        if v.valence:
            varList.append(v.valence[0])
        else: varList.append("")
        if v.activity:
            varList.append(v.activity[0])
        else: varList.append("")
        if v.screen:
            varList.append(v.screen[0])
        else: varList.append("")
        if v.call:
            varList.append(v.call[0])
        else: varList.append("")
        if v.sms:
            varList.append(v.sms[0])
        else: varList.append("")
        if v.builtin:
            varList.append(v.builtin[0])
        else: varList.append("")
        if v.communication:
            varList.append(v.communication[0])
        else: varList.append("")
        if v.entertainment:
            varList.append(v.entertainment[0])
        else: varList.append("")
        if v.finance:
            varList.append(v.finance[0])
        else: varList.append("")
        if v.game:
            varList.append(v.game[0])
        else: varList.append("")
        if v.office:
            varList.append(v.office[0])
        else: varList.append("")
        if v.other:
            varList.append(v.other[0])
        else: varList.append("")
        if v.social:
            varList.append(v.social[0])
        else: varList.append("")
        if v.travel:
            varList.append(v.travel[0])
        else: varList.append("")
        if v.unknown:
            varList.append(v.unknown[0])
        else: varList.append("")
        if v.utilities:
            varList.append(v.utilities[0])
        else: varList.append("")
        if v.weather:
            varList.append(v.weather[0])
        else: varList.append("")
        if v.moodDayX:
            varList.append(v.moodDayX[0])
        else: varList.append("")
        pandasDict[k] = varList
    return pandasDict


def makePanda(listDict):
    # data = pd.DataFrame.from_dict((v) for k,v in dailyGroupedData.items())
    # data = pd.DataFrame.from_dict(list(dailyGroupedData.items()))
    # print(pd.DataFrame.from_dict(a for a in dir(dailyGroupedData.items()) if not a.startswith('__')))
    # for k ,v in dailyGroupedData.items():
    #    print(pd.DataFrame.from_dict(a for a in dir(v) if not a.startswith('__')))

    df = pd.DataFrame(listDict)
    # print(df.transpose())
    joe = df.transpose()
    joe.to_csv("C:/Users/Kylie/Documents/school/Data Mining/output.csv", sep = ",", header= ["id","firstDay", "amountOfDays", "mood", "arousal", "valence",
                                          "activity", "screen", "call", "sms", "builtin", "communication",
                                          "entertainment", "finance", "game", "office", "other", "social",
                                          "travel", "unknown", "utilities", "weather", "moodDayX"])




def fromDateToString(stringDate):
    #y = datetime.strptime(stringDate, "%Y-%m-%d %H:%M:%S.%f")
    return(stringDate.strftime("%Y-%m-%d %H:%M:%S.%f"))


#
# def create_benchmark():
#     new = data2.groupby(data2.day).mean()
#     for i in range(1, len(ts.index)):
#         new.ix[i] = ts.ix[i - 1]
# #

def getPrediction():
    '''
    Returns the actuals and predictions for the benchmark.
    If there is no mood score for 'day-1' or there is no mood score for 'day' we do not return a prediction
    '''
    actuals = []
    predictions = []

    allMoods  =[]
    for k, v in dailyGroupedData.items():
        if v.mood:
            allMoods.append(v.mood[0])
    avgMood = np.mean(allMoods)
    for (idKey, dateKey), v in dailyGroupedData.items():
        if v.moodDayX:
            if v.mood == []:
                v.mood = [avgMood]
            # the prediction can be made, because the mood for 'tomorrow' is known
            actuals.append(v.moodDayX)
            predictions.append(v.mood)
    return (actuals, predictions)

def get_benchmark_score():
    (actuals, predictions) = getPrediction()
    scorer = metrics.median_absolute_error
    return scorer(actuals, predictions)